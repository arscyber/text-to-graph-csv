# README #

text-to-graph-csv.py is a script created by the ArsCyber trademark. It is currently in its early alpha version.
It allows one to convert text files into graphs encoded in a .csv format.

### Uses ###

This script is very useful for the Gephi software. It can be used to visualize text networks and semantic networks. 
* [Gephi](https://en.wikipedia.org/wiki/Gephi)