"""
text-to-graph-csv.py

This is a text to CSV graph converter.


ArsCyber™ Software Trademark
© Copyright
All rights reserved.

This software is in its early alpha stage.
"""


import re

CHUNK_SIZE = 1 #Split the text to N chunks. Larger chunking leads to more coherent but less creative text, as well as longer outputs.
TEXT_DATASET_LOCATION = "inputtext.txt" #Text dataset location
GRAPH_DATASET_LOCATION = "graphoutput.csv"

n_chunk = []
graph = {}


#Parse text
text = ""
f = open(TEXT_DATASET_LOCATION, encoding="utf8")
text = f.read()
f.close()


text = text.replace("\r", "")
text = re.sub(r'\n+', ' ', text)


def splitTheText():

    global splitText
    global n_chunk
    global CHUNK_SIZE
    
    splitText = text.split(" ")
    

    for i in range(0, len(splitText), CHUNK_SIZE):
        n_chunk.append(" ".join(splitText[i:i+CHUNK_SIZE]))


def buildGraph():

    global graph
    global n_chunk
    global splitText
    
    #Load the graph
    for i in range(0, len(n_chunk)):

        if i < (len(n_chunk)-1):
        
            if n_chunk.count(splitText[i]) == 1:
                graph[n_chunk[i]] = [n_chunk[i+1]]
                pass
            else:
                if splitText[i] in graph:
                    graph[n_chunk[i]].append(n_chunk[i+1])
                else:
                    graph[n_chunk[i]] = [n_chunk[i+1]]

        else:
            graph[n_chunk[i]] = []


def buildCsv():

    file = open(GRAPH_DATASET_LOCATION, "w")

    for v in graph:
        for v2 in graph[v]:
            if v is not "" and v2 is not "":
                file.write(v + "," + v2 + "\n")
    
    file.close()


def main():

    print("Converting text to graph...\n")
    splitTheText()
    buildGraph()
    print("Building the CSV file...\n")
    buildCsv()
    
    print("The CSV file has been built...\n")


if __name__ == "__main__":
    main()
